import Vue from "vue";

const data = {
  mouseStartAtY: 0,
  mouseOffsetY: 0,
  mouseBoundLoc: [0, 200],
  mouseCompLocY: 0,
  mouseButtonDown: false,
  vNode: null,
  el: null
};

const emit = (vnode, name, data) => {
  const handlers =
    (vnode.data && vnode.data.on) ||
    (vnode.componentOptions && vnode.componentOptions.listeners);

  if (handlers && handlers[name]) {
    handlers[name].fns(data);
  }
};

const directiveListener = {
  onMouseDown: function(ev) {
    data.mouseButtonDown = true;
    data.mouseStartAtY = ev.clientY;
    data.mouseCompLocY = ev.target.offsetTop;
  },
  onMouseUp: function() {
    data.mouseButtonDown = false;
  },
  onMouseMove: function(ev) {
    if (!(ev.buttons & 1)) {
      data.mouseButtonDown = false;
    }
    if (data.mouseButtonDown) {
      data.mouseOffsetY = ev.clientY - data.mouseStartAtY;
      const computedTop = data.mouseCompLocY + data.mouseOffsetY;
      if (
        computedTop >= data.mouseBoundLoc[0] &&
        computedTop <= data.mouseBoundLoc[1]
      ) {
        emit(data.vNode, "scrolled", computedTop);
        document.getElementById(data.el).style.top = computedTop + "px";
      }
    }
  }
};

export default Vue.directive("drag", {
  bind: function(el, binding, vnode) {
    el.addEventListener("mousedown", directiveListener.onMouseDown);
    el.addEventListener("mouseup", directiveListener.onMouseUp);
    document.addEventListener("mousemove", directiveListener.onMouseMove);
    data.vNode = vnode;
    if (!el.id) {
      const newId = "mouse_drag_directive_" + Math.random();
      el.id = newId;
    }
    data.el = el.id;
    data.mouseBoundLoc = binding.value;
  },
  unbind: function(el) {
    el.removeEventListener("mousedown", directiveListener.onMouseDown);
    el.removeEventListener("mouseup", directiveListener.onMouseUp);
    document.removeEventListener("mousemove", directiveListener.onMouseMove);
  }
});
